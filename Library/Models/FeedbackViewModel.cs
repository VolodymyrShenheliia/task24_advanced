﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Library.Models
{
    public class FeedbackViewModel
    {
        [Required (ErrorMessage= "Please enter your name")]
        [StringLength(10, MinimumLength= 3)]
        public string AuthorName { get; set; }
        
        [Required (ErrorMessage= "Please enter the text of your feedback")]
        [Remote("ValidateInputLength", "Guest")]
        public string Body { get; set; }
        
        public DateTime CreatedAt { get; } = DateTime.Now;
        
        public FeedbackViewModel()
        {
            
        }
        
        public FeedbackViewModel(string authorName, string body, DateTime createdAt)
        {
            AuthorName = authorName;
            Body = body;
            CreatedAt = createdAt;
        }
    }
}