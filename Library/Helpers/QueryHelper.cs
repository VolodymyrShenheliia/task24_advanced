﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Activation;

namespace Library.Helpers
{
    public static class QueryHelper
    {
        public static IEnumerable<int> AnswersId(this HttpRequestBase requestBase)
        {
            return requestBase.QueryString.AllKeys.Where(key => int.TryParse(key, out _))
                .Select(key => requestBase.QueryString[key]?.Split(',').ToList())
                .SelectMany(x => x).ToList().Select(int.Parse).ToList();
        }
    }
}