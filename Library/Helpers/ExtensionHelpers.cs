﻿using System.Collections.Generic;
using System.Web.Mvc;
using BLL.DTO;

namespace Library.Helpers
{
    public static class ExtensionHelpers
    {
        public static MvcHtmlString UnorderedList(this HtmlHelper htmlHelper, IEnumerable<AnswerDto> answers)
        {
            var ulElement = new TagBuilder("ul");
            foreach (var answer in answers)
            {
                var liElement = new TagBuilder("li");
                liElement.SetInnerText($"{answer.Text} - {answer.CheckedTimes}");
                ulElement.InnerHtml += liElement.ToString();
            }

            return new MvcHtmlString(ulElement.ToString());
        }
    }
}