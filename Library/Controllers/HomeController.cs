﻿using System.Web.Mvc;
using BLL.Interfaces;

namespace Library.Controllers
{
    /// <summary>
    /// <c>HomeController</c> is a class.
    /// Contains a method that returns main page.
    /// </summary>
    /// <remarks>
    /// This class can show a page with articles previews.
    /// </remarks>
    public class HomeController : Controller
    {
        private readonly IArticleService _articleService;
        
        public HomeController(IArticleService serv)
        {
            _articleService = serv;
        }
        
        /// <summary>
        /// This method returns main page.
        /// </summary>
        /// <response code="200">Returns all articles previews.</response>
        public ActionResult Index()
        {
            return View(_articleService.GetArticles());
        }
    }
}