﻿using System.Web.Mvc;
using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Library.Models;

namespace Library.Controllers
{
    /// <summary>
    /// <c>GuestController</c> is a class.
    /// Contains a methods to give ability to left and view feedbacks.
    /// </summary>
    /// <remarks>
    /// This class is useful to interact with guest.
    /// </remarks>
    public class GuestController : Controller
    {
        private readonly IFeedbackService _feedbackService;
        private readonly IMapper _mapper;
        
        public GuestController(IFeedbackService serv)
        {
            _feedbackService = serv;
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<FeedbackViewModel, FeedbackDto>().ReverseMap();
            }).CreateMapper();
        }

        /// <summary>
        /// This method returns page with all feedbacks.
        /// </summary>
        /// <response code="200">Returns all feedbacks.</response>
        /// <response code="302">Redirects to feedback form.</response>
        public ActionResult Index()
        {
            ViewBag.Feedbacks = _feedbackService.GetFeedbacks();
            return View();
        }
        
        /// <summary>
        /// This method returns page with form to leave your feedbacks.
        /// </summary>
        /// <response code="200">Returns feedback form.</response>
        [HttpGet]
        public ActionResult PostComment()
        {
            return View();
        }

        /// <summary>
        /// This method returns page with form to correct your feedbacks or redirects to main page.
        /// </summary>
        /// <response code="200">Returns feedback form to correct.</response>
        /// <response code="302">Redirects to guest main page.</response>
        [HttpPost]
        public ActionResult PostComment(FeedbackViewModel feedbackViewModel)
        {
            if (ModelState.IsValid)
            {
               var feedbackDto = _mapper.Map<FeedbackDto>(feedbackViewModel);
                _feedbackService.CreateFeedback(feedbackDto);
                
                return RedirectToAction("Index");
            }

            return View(feedbackViewModel);
        }
        
        /// <summary>
        /// This method checks if the length of feedback body between 50 and 500 symbols.
        /// </summary>
        /// <response code="200">Returns validation message.</response>
        [HttpGet]
        public JsonResult ValidateInputLength(string body)
        {
            return body.Length < 50 || body.Length > 500 ? 
                Json("Incorrect length of your feedback, it is too short(((",JsonRequestBehavior.AllowGet) :
                Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}