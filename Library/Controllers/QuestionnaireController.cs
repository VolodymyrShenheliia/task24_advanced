﻿using System.Web.Mvc;
using BLL.Interfaces;
using Library.Helpers;

namespace Library.Controllers
{
    /// <summary>
    /// <c>GuestController</c> is a class.
    /// Contains a methods to pass questionnaire and to look its statistics.
    /// </summary>
    /// <remarks>
    /// This class can is useful to pass questionnaire.
    /// </remarks>
    public class QuestionnaireController : Controller
    {
        private readonly IQuestionService _questionService;
        private readonly IAnswerService _answerService;
        
        public QuestionnaireController(IQuestionService questionService, IAnswerService answerService)
        {
            _questionService = questionService;
            _answerService = answerService;
        }
        
        /// <summary>
        /// This method returns another action of controller.
        /// </summary>
        /// <response code="200">Returns view depending on result of another action.</response>
        public ActionResult Index()
        {
            return View(_questionService.GetQuestions());
        }

        /// <summary>
        /// This method returns page with questionnaire form.
        /// </summary>
        /// <response code="200">Returns questionnaire form to correct.</response>
        /// <response code="302">Redirects to questionnaire statistics page.</response>
        [HttpGet]
        public ActionResult QuestionnaireResult()
        {
            var queryParamsList = Request.AnswersId();

            foreach (var id in queryParamsList)
                _answerService.Checked(id);

            return QuestionnaireResult(Request.QueryString["Name"]);
        }
        
        /// <summary>
        /// This method returns page with questionnaire statistics.
        /// </summary>
        /// <response code="200">Returns questionnaire statistics.</response>
        [HttpPost]
        public ActionResult QuestionnaireResult(string name)
        {
            ViewBag.Name = name;

            return View(_questionService.GetQuestions());
        }
    }
}