﻿using BLL.Interfaces;
using BLL.Services;
using Ninject.Modules;

namespace Library.Util
{
    public class ServicesModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAnswerService>().To<AnswerService>();
            Bind<IArticleService>().To<ArticleService>();
            Bind<IFeedbackService>().To<FeedbackService>();
            Bind<IQuestionService>().To<QuestionService>();
        }
    }
}