﻿using System.Collections.Generic;
using BLL.DTO;

namespace BLL.Interfaces
{
    public interface IQuestionService
    {
        QuestionDto Get(int? id);
        IEnumerable<QuestionDto> GetQuestions();
        void Dispose();
    }
}