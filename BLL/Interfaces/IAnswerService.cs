﻿using System.Collections.Generic;
using BLL.DTO;

namespace BLL.Interfaces
{
    public interface IAnswerService
    {
        AnswerDto Get(int? id);
        IEnumerable<AnswerDto> GetAnswers();
        void Checked(int id);
        void Dispose();
    }
}