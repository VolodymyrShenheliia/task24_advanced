﻿using System.Collections.Generic;
using BLL.DTO;

namespace BLL.Interfaces
{
    public interface IFeedbackService
    {
        FeedbackDto Get(int? id);
        IEnumerable<FeedbackDto> GetFeedbacks();
        void CreateFeedback(FeedbackDto feedbackDto);
        void Dispose();
    }
}