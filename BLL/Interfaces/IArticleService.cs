﻿using System.Collections.Generic;
using BLL.DTO;

namespace BLL.Interfaces
{
    public interface IArticleService
    {
        ArticleDto Get(int? id);
        IEnumerable<ArticleDto> GetArticles();
        void Dispose();
    }
}