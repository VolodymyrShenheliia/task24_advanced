﻿using System.Collections.Generic;
using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services
{
    public class AnswerService : IAnswerService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;
 
        public AnswerService(IUnitOfWork db)
        {
            _db = db;
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Answer, AnswerDto>().ReverseMap();
            }).CreateMapper();
        }
        
        public AnswerDto Get(int? id)
        {
            if (id == null)
                throw new ValidationException("Id doesn't set","");
            var answer = _db.Answers.Get(id.Value);
            if (answer == null)
                throw new ValidationException("Answer wasn't find","");

            return _mapper.Map<AnswerDto>(answer);
        }

        public IEnumerable<AnswerDto> GetAnswers()
        {
            return _mapper.Map<IEnumerable<AnswerDto>>(_db.Answers.GetAll());
        }

        public void Checked(int id)
        {
            var answer = _db.Answers.Get(id);
            
            if (answer == null)
                throw new ValidationException("Answer wasn't find","");

            answer.CheckedTimes++;
            
            _db.Answers.Update(answer);
            _db.Save();
        }


        public void Dispose()
        {
            _db.Dispose();
        }
    }
}