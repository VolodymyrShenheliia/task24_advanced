﻿using System.Collections.Generic;
using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;
 
        public FeedbackService(IUnitOfWork db)
        {
            _db = db;
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Feedback, FeedbackDto>().ReverseMap();
            }).CreateMapper();
        }
        
        public FeedbackDto Get(int? id)
        {
            if (id == null)
                throw new ValidationException("Id doesn't set","");
            var feedback = _db.Feedbacks.Get(id.Value);
            if (feedback == null)
                throw new ValidationException("Feedback wasn't find","");

            return _mapper.Map<FeedbackDto>(feedback);
        }

        public IEnumerable<FeedbackDto> GetFeedbacks()
        {
            return _mapper.Map<IEnumerable<FeedbackDto>>(_db.Feedbacks.GetAll());
        }
        
        public void CreateFeedback(FeedbackDto feedbackDto)
        {
            var feedback = _mapper.Map<Feedback>(feedbackDto);
            
            _db.Feedbacks.Create(feedback);
            _db.Save();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}