﻿using System.Collections.Generic;
using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;
 
        public QuestionService(IUnitOfWork db)
        {
            _db = db;
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Question, QuestionDto>().ReverseMap();
                cfg.CreateMap<Answer, AnswerDto>().ReverseMap();
            }).CreateMapper();
        }
        
        public QuestionDto Get(int? id)
        {
            if (id == null)
                throw new ValidationException("Id doesn't set","");
            var question = _db.Questions.Get(id.Value);
            if (question == null)
                throw new ValidationException("Question wasn't find","");

            return _mapper.Map<QuestionDto>(question);
        }

        public IEnumerable<QuestionDto> GetQuestions()
        {
            return _mapper.Map<IEnumerable<QuestionDto>>(_db.Questions.GetAll());
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}