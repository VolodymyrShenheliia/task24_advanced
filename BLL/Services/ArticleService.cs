﻿using System.Collections.Generic;
using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;
 
        public ArticleService(IUnitOfWork db)
        {
            _db = db;
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Article, ArticleDto>().ReverseMap();
            }).CreateMapper();
        }
        
        public ArticleDto Get(int? id)
        {
            if (id == null)
                throw new ValidationException("Id doesn't set","");
            var article = _db.Articles.Get(id.Value);
            if (article == null)
                throw new ValidationException("Article wasn't find","");

            return _mapper.Map<ArticleDto>(article);
        }

        public IEnumerable<ArticleDto> GetArticles()
        {
            return _mapper.Map<IEnumerable<ArticleDto>>(_db.Articles.GetAll());
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}