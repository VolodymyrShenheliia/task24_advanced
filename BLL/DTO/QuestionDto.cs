﻿using System.Collections.Generic;

namespace BLL.DTO
{
    public class QuestionDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public QuestionTypesDto QuestionType { get; set; }
        
        public ICollection<AnswerDto> Answers { get; set; }
        
        public QuestionDto()
        {
            
        }
        
        public QuestionDto(int id, string text, QuestionTypesDto questionType)
        {
            Id = id;
            Text = text;
            QuestionType = questionType;
        }
    }

    public enum QuestionTypesDto
    {
        Radio,
        Check
    }
}