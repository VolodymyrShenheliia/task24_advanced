﻿namespace BLL.DTO
{
    public class ArticleDto
    {
        public int Id { get; set; }
        public string Heading { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        
        public ArticleDto()
        {
            
        }
        
        public ArticleDto(string heading, string description, string body)
        {
            Heading = heading;
            Description = description;
            Body = body;
        }
    }
}