﻿namespace BLL.DTO
{
    public class AnswerDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int CheckedTimes { get; set; }
        
        public int QuestionId { get; set; }
        public QuestionDto QuestionDto { get; set; }

        public AnswerDto()
        {
            
        }

        public AnswerDto(string text, int checkedTimes, int questionId)
        {
            Text = text;
            CheckedTimes = checkedTimes;
            QuestionId = questionId;
        }
    }
}