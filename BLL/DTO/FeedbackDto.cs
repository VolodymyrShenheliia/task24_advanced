﻿using System;

namespace BLL.DTO
{
    public class FeedbackDto
    {
        public int Id { get; set; }
        public string AuthorName { get; set; }
        public string Body { get; set; }
        public DateTime CreatedAt { get; set; }

        public FeedbackDto()
        {
            
        }
        
        public FeedbackDto(string authorName, string body, DateTime createdAt)
        {
            AuthorName = authorName;
            Body = body;
            CreatedAt = createdAt;
        }
    }
}