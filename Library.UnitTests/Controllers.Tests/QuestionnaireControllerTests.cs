﻿using System.Collections.Generic;
using System.Web.Mvc;
using BLL.DTO;
using BLL.Interfaces;
using Library.Controllers;
using Moq;
using NUnit.Framework;

namespace Library.UnitTests.Controllers.Tests
{
    public class QuestionnaireControllerTests
    {
         private List<QuestionDto> _testQuestions;
         private List<AnswerDto> _testAnswers;
         
         [SetUp]
        public void Setup()
        {
            _testQuestions = new List<QuestionDto>
            {
                new QuestionDto(1, "How often do you read books?", QuestionTypesDto.Radio),
                new QuestionDto(2,"What books have you read?", QuestionTypesDto.Check)
            };
            
            _testAnswers = new List<AnswerDto>
            {
                new AnswerDto("Every day", 4, 1),
                new AnswerDto("2-5 times a week", 7, 1),
                new AnswerDto("Once a week", 8, 1),
                new AnswerDto("Once a month", 3, 1),
                new AnswerDto("Don't read", 5, 1),
                new AnswerDto("Anna Karenina", 14, 2),
                new AnswerDto("Madame Bovary", 2, 2),
                new AnswerDto("War and Peace", 18, 2),
                new AnswerDto("The Great Gatsby", 13, 2),
                new AnswerDto("Lolita", 2, 2)
            };
        }

        [Test]
        public void QuestionnaireHttpGetIndex_NotNullResult()
        {
            var answerService = new Mock<IAnswerService>();
            var questionService = new Mock<IQuestionService>();

            answerService.Setup(x => x.GetAnswers()).Returns(_testAnswers);
            questionService.Setup(x => x.GetQuestions()).Returns(_testQuestions);

            var questionnaireController = new QuestionnaireController(questionService.Object, answerService.Object);

            var viewResult = questionnaireController.Index() as ViewResult;
            
            Assert.IsNotNull(viewResult);
        }
    }
}