﻿using System.Collections.Generic;
using System.Web.Mvc;
using BLL.DTO;
using BLL.Interfaces;
using Library.Controllers;
using Moq;
using NUnit.Framework;

namespace Library.UnitTests.Controllers.Tests
{
    [TestFixture]
    public class HomeControllerTests
    {
        private List<ArticleDto> _articles;

        [SetUp]
        public void Setup()
        {
            _articles = new List<ArticleDto>
            {
                new ArticleDto("TITLE HEADING 1", "Title description, Apr 23, 2021", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."), 
                new ArticleDto("TITLE HEADING 2", "Title description, Apr 22, 2021", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." ),
                new ArticleDto("TITLE HEADING 3", "Title description, Apr 21, 2021", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." ), 
                new ArticleDto("TITLE HEADING 4", "Title description, Apr 20, 2021", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." )
            };
        }

        [Test]
        public void HomeHttpGetIndex_NotNullResult()
        {
            var articlesService = new Mock<IArticleService>();

            articlesService.Setup(x => x.GetArticles()).Returns(_articles);

            var homeController = new HomeController(articlesService.Object);
            
            var viewResult = homeController.Index() as ViewResult;
            
            Assert.IsNotNull(viewResult);
        }
    }
}