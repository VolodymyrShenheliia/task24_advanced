﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BLL.DTO;
using BLL.Interfaces;
using Library.Controllers;
using Moq;
using NUnit.Framework;

namespace Library.UnitTests.Controllers.Tests
{
    [TestFixture]
    public class GuestControllerTests
    {
         private List<FeedbackDto> _testFeedbacks;

        [SetUp]
        public void Setup()
        {
            _testFeedbacks = new List<FeedbackDto>
            {
                new FeedbackDto("Tom Black", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", DateTime.Now ), 
                new FeedbackDto("Richard Simons", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", DateTime.Now )
            };
        }
        
        [Test]
        public void StartGetIndex_ExpectNotNullResult()
        {
            var feedbackService = new Mock<IFeedbackService>();
            feedbackService.Setup(x => x.GetFeedbacks()).Returns(_testFeedbacks);

            var guestController = new GuestController(feedbackService.Object);
            
            var result = guestController.Index() as ViewResult;
            
            Assert.IsNotNull(guestController);
        }
    }
}