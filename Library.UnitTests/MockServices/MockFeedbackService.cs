﻿using System.Collections.Generic;
using System.Linq;
using BLL.DTO;
using BLL.Interfaces;

namespace Library.UnitTests.MockServices
{
    public class MockFeedbackService : IFeedbackService
    {
        private readonly List<FeedbackDto> _testFeedback;
        
        public MockFeedbackService(List<FeedbackDto> testFeedback)
        {
            _testFeedback = testFeedback;
        }
        
        public FeedbackDto Get(int? id)
        {
            return _testFeedback.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<FeedbackDto> GetFeedbacks()
        {
            return _testFeedback;
        }

        public void CreateFeedback(FeedbackDto feedbackDto)
        {
            throw new System.NotImplementedException();
        }

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }
    }
}