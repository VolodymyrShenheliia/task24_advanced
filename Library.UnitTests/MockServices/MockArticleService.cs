﻿using System.Collections.Generic;
using System.Linq;
using BLL.DTO;
using BLL.Interfaces;

namespace Library.UnitTests.MockServices
{
    public class MockArticleService : IArticleService
    {
        private readonly List<ArticleDto> _testArticle;
        
        public MockArticleService(List<ArticleDto> testArticle)
        {
            _testArticle = testArticle;
        }
        
        public ArticleDto Get(int? id)
        {
            return _testArticle.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<ArticleDto> GetArticles()
        {
            return _testArticle;
        }

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }
    }
}