﻿using System.Collections.Generic;
using System.Linq;
using BLL.DTO;
using BLL.Interfaces;

namespace Library.UnitTests.MockServices
{
    public class MockAnswerService : IAnswerService
    {
        private readonly List<AnswerDto> _testAnswers;
        
        public MockAnswerService(List<AnswerDto> testAnswers)
        {
            _testAnswers = testAnswers;
        }
        
        public AnswerDto Get(int? id)
        {
            return _testAnswers.FirstOrDefault(answer => answer.Id == id);
        }

        public IEnumerable<AnswerDto> GetAnswers()
        {
            return _testAnswers;
        }

        public void Checked(int id)
        {
            throw new System.NotImplementedException();
        }

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }
    }
}