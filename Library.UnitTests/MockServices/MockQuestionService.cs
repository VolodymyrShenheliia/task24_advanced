﻿using System.Collections.Generic;
using System.Linq;
using BLL.DTO;
using BLL.Interfaces;

namespace Library.UnitTests.MockServices
{
    public class MockQuestionService : IQuestionService
    {
        private readonly List<QuestionDto> _testQuestion;
        
        public MockQuestionService(List<QuestionDto> testQuestion)
        {
            _testQuestion = testQuestion;
        }
        
        public QuestionDto Get(int? id)
        {
            return _testQuestion.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<QuestionDto> GetQuestions()
        {
            return _testQuestion;
        }

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }
    }
}