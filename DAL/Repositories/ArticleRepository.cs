﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class ArticleRepository : IRepository<Article>
    {
        private readonly LibraryContext _db;
 
        public ArticleRepository(LibraryContext context)
        {
            _db = context;
        }
 
        public IEnumerable<Article> GetAll()
        {
            return _db.Articles;
        }
 
        public Article Get(int id)
        {
            return _db.Articles.Find(id);
        }
 
        public void Create(Article article)
        {
            _db.Articles.Add(article);
        }
 
        public void Update(Article article)
        {
            _db.Entry(article).State = EntityState.Modified;
        }
        
        public IEnumerable<Article> Find(Func<Article, bool> predicate)
        {
            return _db.Articles.Where(predicate).ToList();
        }
        
        public void Delete(int id)
        {
            var article = _db.Articles.Find(id);
            if (article != null)
                _db.Articles.Remove(article);
        }
    }
}