﻿using System;
using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public sealed class EfUnitOfWork : IUnitOfWork
    {
        private readonly LibraryContext _db;
        private AnswerRepository _answerRepository;
        private ArticleRepository _articleRepository;
        private QuestionRepository _questionRepository;
        private FeedbackRepository _feedbackRepository;
 
        public EfUnitOfWork(string connectionString)
        {
            _db = new LibraryContext(connectionString);
        }
        
        public IRepository<Answer> Answers => _answerRepository ?? (_answerRepository = new AnswerRepository(_db));
        public IRepository<Article> Articles => _articleRepository ?? (_articleRepository = new ArticleRepository(_db));
        public IRepository<Feedback> Feedbacks => _feedbackRepository ?? (_feedbackRepository = new FeedbackRepository(_db));
        public IRepository<Question> Questions => _questionRepository ?? (_questionRepository = new QuestionRepository(_db));
        
        public void Save()
        {
            _db.SaveChanges();
        }
 
        private bool _disposed;

        private void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _db.Dispose();
            }
            _disposed = true;
        }
 
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}