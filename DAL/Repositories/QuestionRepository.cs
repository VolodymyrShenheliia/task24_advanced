﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class QuestionRepository : IRepository<Question>
    {
        private readonly LibraryContext _db;
 
        public QuestionRepository(LibraryContext context)
        {
            _db = context;
        }
 
        public IEnumerable<Question> GetAll()
        {
            return _db.Questions.Include(question => question.Answers);
        }
 
        public Question Get(int id)
        {
            return _db.Questions.Find(id);
        }
 
        public void Create(Question question)
        {
            _db.Questions.Add(question);
        }
 
        public void Update(Question question)
        {
            _db.Entry(question).State = EntityState.Modified;
        }
        
        public IEnumerable<Question> Find(Func<Question, bool> predicate)
        {
            return _db.Questions.Include(question => question.Answers).AsEnumerable().Where(predicate).ToList();
        }
        
        public void Delete(int id)
        {
            var question = _db.Questions.Find(id);
            if (question != null)
                _db.Questions.Remove(question);
        }
    }
}