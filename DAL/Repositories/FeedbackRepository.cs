﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class FeedbackRepository : IRepository<Feedback>
    {
        private readonly LibraryContext _db;
 
        public FeedbackRepository(LibraryContext context)
        {
            _db = context;
        }
 
        public IEnumerable<Feedback> GetAll()
        {
            return _db.Feedbacks;
        }
 
        public Feedback Get(int id)
        {
            return _db.Feedbacks.Find(id);
        }
 
        public void Create(Feedback feedback)
        {
            _db.Feedbacks.Add(feedback);
        }
 
        public void Update(Feedback feedback)
        {
            _db.Entry(feedback).State = EntityState.Modified;
        }
        
        public IEnumerable<Feedback> Find(Func<Feedback, bool> predicate)
        {
            return _db.Feedbacks.Where(predicate).ToList();
        }
        
        public void Delete(int id)
        {
            var feedback = _db.Feedbacks.Find(id);
            if (feedback != null)
                _db.Feedbacks.Remove(feedback);
        }
    }
}