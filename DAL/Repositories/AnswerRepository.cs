﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class AnswerRepository : IRepository<Answer>
    {
        private readonly LibraryContext _db;
 
        public AnswerRepository(LibraryContext context)
        {
            this._db = context;
        }
 
        public IEnumerable<Answer> GetAll()
        {
            return _db.Answers.Include(o => o.Question);
        }
 
        public Answer Get(int id)
        {
            return _db.Answers.Find(id);
        }
 
        public void Create(Answer answer)
        {
            _db.Answers.Add(answer);
        }
 
        public void Update(Answer answer)
        {
            _db.Entry(answer).State = EntityState.Modified;
        }
        
        public IEnumerable<Answer> Find(Func<Answer, bool> predicate)
        {
            return _db.Answers.Include(answer => answer.Question).AsEnumerable().Where(predicate).ToList();
        }
        
        public void Delete(int id)
        {
            var answer = _db.Answers.Find(id);
            if (answer != null)
                _db.Answers.Remove(answer);
        }
    }
}