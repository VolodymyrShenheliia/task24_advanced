﻿using System.Collections.Generic;

namespace DAL.Entities
{
    public class Question : BaseEntity
    {
        public string Text { get; set; }
        public QuestionTypes QuestionType { get; set; }
        
        public ICollection<Answer> Answers { get; set; }

        
        public Question()
        {
            
        }
        
        public Question(int id, string text, QuestionTypes questionType)
        {
            Id = id;
            Text = text;
            QuestionType = questionType;
        }
    }

    public enum QuestionTypes
    {
        Radio,
        Check
    }
}