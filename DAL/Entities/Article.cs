﻿namespace DAL.Entities
{
    public class Article : BaseEntity
    {
        public string Heading { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        
        public Article()
        {
            
        }
        
        public Article(string heading, string description, string body)
        {
            Heading = heading;
            Description = description;
            Body = body;
        }
    }
}