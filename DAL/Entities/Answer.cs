﻿namespace DAL.Entities
{
    public class Answer : BaseEntity
    {
        public string Text { get; set; }
        public int CheckedTimes { get; set; }
        
        public int QuestionId { get; set; }
        public Question Question { get; set; }

        public Answer()
        {
            
        }

        public Answer(string text, int checkedTimes, int questionId)
        {
            Text = text;
            CheckedTimes = checkedTimes;
            QuestionId = questionId;
        }
    }
}