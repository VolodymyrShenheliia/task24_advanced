﻿using System;

namespace DAL.Entities
{
    public class Feedback : BaseEntity
    {
        public string AuthorName { get; set; }
        public string Body { get; set; }
        public DateTime CreatedAt { get; set; }

        public Feedback()
        {
            
        }
        
        public Feedback(string authorName, string body, DateTime createdAt)
        {
            AuthorName = authorName;
            Body = body;
            CreatedAt = createdAt;
        }
    }
}