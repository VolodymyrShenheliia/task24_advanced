﻿using System;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Answer> Answers { get; }
        IRepository<Article> Articles { get; }
        IRepository<Feedback> Feedbacks { get; }
        IRepository<Question> Questions { get; }
        void Save();
    }
}

    