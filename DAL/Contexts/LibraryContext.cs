﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using DAL.Entities;

namespace DAL.Contexts
{
    public class LibraryContext : DbContext
    {
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<Question> Questions { get; set; }
 
        static LibraryContext()
        {
            Database.SetInitializer(new LibraryDbInitializer());
        }
        
        public LibraryContext(string connectionString) : base(connectionString)
        {
            
        }
    }
     
    public class LibraryDbInitializer : DropCreateDatabaseIfModelChanges<LibraryContext>
    {
        protected override void Seed(LibraryContext db)
        {
            var articles = new List<Article>
            {
                new Article("TITLE HEADING 1", "Title description, Apr 23, 2021", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."), 
                new Article("TITLE HEADING 2", "Title description, Apr 22, 2021", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." ),
                new Article("TITLE HEADING 3", "Title description, Apr 21, 2021", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." ), 
                new Article("TITLE HEADING 4", "Title description, Apr 20, 2021", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." )
            };
            
            var questions = new List<Question>
            {
                new Question(1, "How often do you read books?", QuestionTypes.Radio),
                new Question(2,"What books have you read?", QuestionTypes.Check)
            };

            var answers = new List<Answer>
            {
                new Answer("Every day", 4, 1),
                new Answer("2-5 times a week", 7, 1),
                new Answer("Once a week", 8, 1),
                new Answer("Once a month", 3, 1),
                new Answer("Don't read", 5, 1),
                new Answer("Anna Karenina", 14, 2),
                new Answer("Madame Bovary", 2, 2),
                new Answer("War and Peace", 18, 2),
                new Answer("The Great Gatsby", 13, 2),
                new Answer("Lolita", 2, 2)
            };
            
            
            var feedbacks =  new List<Feedback>
            {
                new Feedback("Tom Black", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", DateTime.Now ), 
                new Feedback("Richard Simons", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", DateTime.Now )
            };
            
            db.Articles.AddRange(articles);
            db.Answers.AddRange(answers);
            db.Feedbacks.AddRange(feedbacks);
            db.Questions.AddRange(questions);
            
            db.SaveChanges();
        }
    }
}